install & configuration cert-manager

```bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.crds.yaml

```
![](https://i.imgur.com/sRSB3ih.png)

![](https://i.imgur.com/61rtsOL.png)


Pour vérifier les ressources

```bash
cat <<EOF > test-resources.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: cert-manager-test
---
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: test-selfsigned
  namespace: cert-manager-test
spec:
  selfSigned: {}
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: selfsigned-cert
  namespace: cert-manager-test
spec:
  dnsNames:
    - example.com
  secretName: selfsigned-cert-tls
  issuerRef:
    name: test-selfsigned
EOF
```

creer la ressource test

```bash
kubectl apply -f test-resources.yaml
```

verifier le statut des nouveaux certificats

```bash
kubectl describe certificate -n cert-manager-test
```

clean up les ressources

```bash
kubectl delete -f test-resources.yaml
```

confiigurer sur le cluster :

https://cert-manager.io/v1.1-docs/configuration/