# Projet Devops

Our goal is to setup following infrastructure:

| Hostname   | IP          |
| ---------- | ----------- |
| k8s-master | 192.168.1.7 |
| k8s-node   | 192.168.1.8 |

## Installation du cluster kubeadm on both vm

You'll need two vm. The first one will be the kube master and the second one the kube node.

We choose to create 2 vm on centos 7 distribution. 

Each one will need this hardware requirement: 2 CPU and 2 GB RAM.

Each one have one networking adapter branch on bridge and another one on host only.

### Step 1 : Set and update the hostname 

Add in /etc/hosts file for local name resolution. 

```bash
[root@localhost ~]# cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.56.7 k8s-master
192.168.56.8 k8s-node
[root@localhost ~]# sudo hostname k8s-<choose master or node>
```

### Step 2 : Disable swap 

```bash
[root@localhost ~]# cat /etc/fstab
#
# /etc/fstab
# Created by anaconda on Wed Dec  8 15:09:24 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=d4c44571-008e-465e-84af-07dae47dda7d /boot                   xfs     defaults        0 0
#/dev/mapper/centos-swap swap 

[root@localhost ~]# swapoff -a
```

### Step 3 : Add firewall rules for Kubernetes service endpoint & kubelet on both vm

```bash
firewall-cmd --permanent --add-port=6443/tcp 
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --permanent --reload

[root@localhost ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: dhcpv6-client ssh
  ports: 6443/tcp 10250/tcp 22/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 

```

### Step 4 : Let iptables see bridged traffic

```bash
 cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf 
 net.bridge.bridge-nf-call-ip6tables = 1 
 net.bridge.bridge-nf-call-iptables = 1 
 EOF
 sysctl --system
```

### Step 5 : Add kubernetes repository

```bash
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo 
[kubernetes] 
name=Kubernetes 
baseurl=[https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch](https://packages.cloud.google.com/yum/repos/kubernetes-el7-\%24basearch) 
enabled=1 
gpgcheck=1 
repo_gpgcheck=1 
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg 
exclude=kubelet kubeadm kubectl 
EOF
```

### Step 6 : Set SElinux to enforcing mode 

```bash
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```

### Step 7 : Install yum utility 

```bash
 yum utility. yum install -y yum-utils device-mapper-persistent-data lvm2
```

### Step 8 : Add docker repository and install it 

```bash
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo 
yum update -y && yum install containerd.io docker-ce docker-ce-cli
```

### Step 9 : Create a overlay docker configuration file

```bash
 mkdir /etc/docker 
 cat > /etc/docker/daemon.json 
 <<
 EOF 
 { 
 "exec-opts": ["native.cgroupdriver=systemd"], 
 "log-driver": 
 "json-file", 
 "log-opts": { "max-size": "100m" }, 
 "storage-driver": 
 "overlay2", 
 "storage-opts": [ 
 "overlay2.override_kernel_check=true" 
 ] 
 } 
 EOF
```

### Step 10 : Add docker to systemd

```
mkdir -p /etc/systemd/system/docker.service.d 
systemctl daemon-reload 
systemctl restart docker 
systemctl enable docker
```

### Step 11 : Install kubeadm and other important packages

```bash
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes 
systemctl enable --now kubelet
```

## How to Deploy a Kubernetes Cluster

### Initialise the cluster only on kube-master

```bash
kubeadm init --pod-network-cidr 10.244.0.0/16 --apiserver-advertise-address=192.168.99.112 
```

Note : Here 192.168.99.112 is the ip of our hostonly adapter (enp0s8 for us). The **10.244.0.0/16** network value reflects the configuration of the *kube-flannel.yml* file. 

```bash
[root@k8s-master ~]# kubeadm init --apiserver-advertise-address 192.168.56.3 --pod-network-cidr=10.244.0.0/16

[...]
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 192.168.56.7:6443 --token t0s0gu.oqx31nsmpz10sabl \
	--discovery-token-ca-cert-hash sha256:edd5b66f94f9473029de7ec294a1dba07533168b1296ddbc448a2b83c8efffc0 
```

```bash
 mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

### Pod Networking

#### Configuration de l'add-on en charge du réseau pour les pods au sein du cluster

Install Waves works for weaving containers into applications :

```bash
[root@kube-master ~]# kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
serviceaccount/weave-net created
clusterrole.rbac.authorization.k8s.io/weave-net created
clusterrolebinding.rbac.authorization.k8s.io/weave-net created
role.rbac.authorization.k8s.io/weave-net created
rolebinding.rbac.authorization.k8s.io/weave-net created
daemonset.apps/weave-net created
```

#### Install auto-completion

```
yum install bash-completion 
echo "source <(kubectl completion bash)" >> ~/.bashrc
```

### Join the cluster only on kube-node

```bash
[root@kube-node ~]# kubeadm join 192.168.56.7:6443 --token t0s0gu.oqx31nsmpz10sabl \
> --discovery-token-ca-cert-hash sha256:edd5b66f94f9473029de7ec294a1dba07533168b1296ddbc448a2b83c8efffc0 
[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.

```

### Namespace

Like we're less than 10 persons to use our cluster, we decided to create only one namespace for the moment. Of course, we'll can create more later.
If you're more than 10 persons to use your cluster, you can create a cluster for the developpers and another one for the production for example.

Create a new YAML file called `my-namespace.yaml` with the contents:

```bash
[root@kube-master home]# mkdir kubernetes-project
[root@kube-master home]# cd kubernetes-project/
[root@kube-master kubernetes-project]# touch my-namespace.yaml
[root@kube-master kubernetes-project]# vi my-namespace.yaml 
[root@kube-master kubernetes-project]# cat my-namespace.yaml 
apiVersion: v1
kind: Namespace
metadata:
  name: kubernetes-project
[root@kube-master kubernetes-project]# ls
my-namespace.yaml

[root@kube-master kubernetes-project]# kubectl create -f ./my-namespace.yaml
namespace/kubernetes-project created

[root@kube-master kubernetes-project]# kubectl get namespaces
NAME                 STATUS   AGE
default              Active   148m
kube-node-lease      Active   148m
kube-public          Active   148m
kube-system          Active   148m
kubernetes-project   Active   28s
```













### Storage Provisioner

Le cluster doit pouvoir provisionner des disques réseaux pour répondre aux PersistentVolumeClaim des différents services.

Cela peut être en utilisant les ressources du fournisseur cloud (CSI Cinder...) ou avec un opérateur pour gérer le provisioning de volume.

Exemple :

Local Path Provisioner

**2 points**

### RBAC

Le cluster doit présenter une bonne politique de RBAC. Ainsi sur votre cluster il doit y avoir :

un administrateur cluster avec accès complet
 un ops sans accès aux ressources dans la namespace kube-system
 un dev client qui a accès seulement au port-forward, cp et exec pour les pods des namespaces du service Wordpress

Il est possible d'utiliser un opérateur pour faciliter la gestion des RBAC :

rbac-manager
```
kubectl create -f tiller-clusterrolebinding.yaml
helm init --service-account tiller --upgrade

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: administrateur
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: administrateur
    namespace: kube-system
```

**4 points**

### Ingress

Installation d'un contrôleur pour les Ingress, il remplie la fonction de reverse-proxy, c'est un des seuls services exposés.

Exemple :

Ingress NGINX

**2 points**

## Services
 Voici la liste des services imposés. Il faut :

savoir expliquer l'intérêt du service choisir et expliquer la configuration mettre en place sur le cluster

Gestion des certificats HTTPS
 Mise en place de cert-manager afin de gérer les certificats HTTPS automatiquement.

Il est possible de configurer un CA issuer sur cert-manager : https://cert-manager.io/docs/configuration/ca/ Cela peut faciliter la mise en place du HTTPS en ajoutant le certificat racine sur votre navigateur web.
 **4 points**

### Monitoring

## Installation de grafana sur le cluster

On installe le projet kube prometheus:
```
git clone https://github.com/prometheus-operator/kube-prometheus.git
```
On se place dans le repertoire installé:
```
cd ~/kube-prometheus
```

On crée un namespace et autre prerequis:
```
kubectl create -f manifests/setup
```

Les résultats de l'exécution:
```
namespace/monitoring created
customresourcedefinition.apiextensions.k8s.io/alertmanagerconfigs.monitoring.coreos.com created
customresourcedefinition.apiextensions.k8s.io/alertmanagers.monitoring.coreos.com created
customresourcedefinition.apiextensions.k8s.io/podmonitors.monitoring.coreos.com created
customresourcedefinition.apiextensions.k8s.io/probes.monitoring.coreos.com created
customresourcedefinition.apiextensions.k8s.io/prometheuses.monitoring.coreos.com created
customresourcedefinition.apiextensions.k8s.io/prometheusrules.monitoring.coreos.com created
customresourcedefinition.apiextensions.k8s.io/servicemonitors.monitoring.coreos.com created
customresourcedefinition.apiextensions.k8s.io/thanosrulers.monitoring.coreos.com created
clusterrole.rbac.authorization.k8s.io/prometheus-operator created
clusterrolebinding.rbac.authorization.k8s.io/prometheus-operator created
deployment.apps/prometheus-operator created
service/prometheus-operator created
serviceaccount/prometheus-operator created
```
```
kubectl get ns monitoring
NAME         STATUS   AGE
monitoring   Active   2m41s
```
```
kubectl get pods -n monitoring
NAME                                   READY   STATUS    RESTARTS   AGE
prometheus-operator-84dc795dc8-jbgjm   2/2     Running   0          91s
```

On continue le deployment de prometheus si aucune erreur:
```
kubectl create -f manifests/
```
Les résultats de l'exécution:
```
poddisruptionbudget.policy/alertmanager-main created
prometheusrule.monitoring.coreos.com/alertmanager-main-rules created
secret/alertmanager-main created
service/alertmanager-main created
serviceaccount/alertmanager-main created
servicemonitor.monitoring.coreos.com/alertmanager created
clusterrole.rbac.authorization.k8s.io/blackbox-exporter created
clusterrolebinding.rbac.authorization.k8s.io/blackbox-exporter created
configmap/blackbox-exporter-configuration created
deployment.apps/blackbox-exporter created
service/blackbox-exporter created
serviceaccount/blackbox-exporter created
servicemonitor.monitoring.coreos.com/blackbox-exporter created
secret/grafana-datasources created
configmap/grafana-dashboard-alertmanager-overview created
configmap/grafana-dashboard-apiserver created
configmap/grafana-dashboard-cluster-total created
configmap/grafana-dashboard-controller-manager created
configmap/grafana-dashboard-k8s-resources-cluster created
configmap/grafana-dashboard-k8s-resources-namespace created
configmap/grafana-dashboard-k8s-resources-node created
configmap/grafana-dashboard-k8s-resources-pod created
configmap/grafana-dashboard-k8s-resources-workload created
configmap/grafana-dashboard-k8s-resources-workloads-namespace created
configmap/grafana-dashboard-kubelet created
configmap/grafana-dashboard-namespace-by-pod created
configmap/grafana-dashboard-namespace-by-workload created
configmap/grafana-dashboard-node-cluster-rsrc-use created
configmap/grafana-dashboard-node-rsrc-use created
configmap/grafana-dashboard-nodes created
configmap/grafana-dashboard-persistentvolumesusage created
configmap/grafana-dashboard-pod-total created
configmap/grafana-dashboard-prometheus-remote-write created
configmap/grafana-dashboard-prometheus created
configmap/grafana-dashboard-proxy created
configmap/grafana-dashboard-scheduler created
configmap/grafana-dashboard-workload-total created
configmap/grafana-dashboards created
deployment.apps/grafana created
service/grafana created
serviceaccount/grafana created
servicemonitor.monitoring.coreos.com/grafana created
prometheusrule.monitoring.coreos.com/kube-prometheus-rules created
clusterrole.rbac.authorization.k8s.io/kube-state-metrics created
clusterrolebinding.rbac.authorization.k8s.io/kube-state-metrics created
deployment.apps/kube-state-metrics created
prometheusrule.monitoring.coreos.com/kube-state-metrics-rules created
service/kube-state-metrics created
serviceaccount/kube-state-metrics created
servicemonitor.monitoring.coreos.com/kube-state-metrics created
prometheusrule.monitoring.coreos.com/kubernetes-monitoring-rules created
servicemonitor.monitoring.coreos.com/kube-apiserver created
servicemonitor.monitoring.coreos.com/coredns created
servicemonitor.monitoring.coreos.com/kube-controller-manager created
servicemonitor.monitoring.coreos.com/kube-scheduler created
servicemonitor.monitoring.coreos.com/kubelet created
clusterrole.rbac.authorization.k8s.io/node-exporter created
clusterrolebinding.rbac.authorization.k8s.io/node-exporter created
daemonset.apps/node-exporter created
prometheusrule.monitoring.coreos.com/node-exporter-rules created
service/node-exporter created
serviceaccount/node-exporter created
servicemonitor.monitoring.coreos.com/node-exporter created
clusterrole.rbac.authorization.k8s.io/prometheus-adapter created
clusterrole.rbac.authorization.k8s.io/system:aggregated-metrics-reader created
clusterrolebinding.rbac.authorization.k8s.io/prometheus-adapter created
clusterrolebinding.rbac.authorization.k8s.io/resource-metrics:system:auth-delegator created
clusterrole.rbac.authorization.k8s.io/resource-metrics-server-resources created
configmap/adapter-config created
deployment.apps/prometheus-adapter created
poddisruptionbudget.policy/prometheus-adapter created
rolebinding.rbac.authorization.k8s.io/resource-metrics-auth-reader created
service/prometheus-adapter created
serviceaccount/prometheus-adapter created
servicemonitor.monitoring.coreos.com/prometheus-adapter created
clusterrole.rbac.authorization.k8s.io/prometheus-k8s created
clusterrolebinding.rbac.authorization.k8s.io/prometheus-k8s created
prometheusrule.monitoring.coreos.com/prometheus-operator-rules created
servicemonitor.monitoring.coreos.com/prometheus-operator created
poddisruptionbudget.policy/prometheus-k8s created
prometheus.monitoring.coreos.com/k8s created
prometheusrule.monitoring.coreos.com/prometheus-k8s-prometheus-rules created
rolebinding.rbac.authorization.k8s.io/prometheus-k8s-config created
rolebinding.rbac.authorization.k8s.io/prometheus-k8s created
rolebinding.rbac.authorization.k8s.io/prometheus-k8s created
rolebinding.rbac.authorization.k8s.io/prometheus-k8s created
role.rbac.authorization.k8s.io/prometheus-k8s-config created
role.rbac.authorization.k8s.io/prometheus-k8s created
role.rbac.authorization.k8s.io/prometheus-k8s created
role.rbac.authorization.k8s.io/prometheus-k8s created
service/prometheus-k8s created
serviceaccount/prometheus-k8s created
servicemonitor.monitoring.coreos.com/prometheus-k8s created
```

Cela va crée des pods pour le namespace monitoring:
```
kubectl get pods -n monitoring
NAME                                   READY   STATUS    RESTARTS   AGE
alertmanager-main-0                    2/2     Running   0          113s
alertmanager-main-1                    2/2     Running   0          113s
alertmanager-main-2                    2/2     Running   0          113s
blackbox-exporter-6c95587d7-2vf28      3/3     Running   0          113s
grafana-9b54884bf-9s82l                1/1     Running   0          112s
kube-state-metrics-b545789dd-27xg4     3/3     Running   0          111s
node-exporter-cbjx5                    2/2     Running   0          111s
node-exporter-fs2vj                    2/2     Running   0          111s
node-exporter-gswkl                    2/2     Running   0          111s
node-exporter-hxv7l                    2/2     Running   0          111s
node-exporter-ktnd8                    2/2     Running   0          111s
prometheus-adapter-5c977869c-7mhz2     1/1     Running   0          111s
prometheus-adapter-5c977869c-8fndf     1/1     Running   0          111s
prometheus-k8s-0                       2/2     Running   1          109s
prometheus-k8s-1                       2/2     Running   1          109s
prometheus-operator-84dc795dc8-jbgjm   2/2     Running   0          7m37s
```

Et plusieur service vont etre crée aussi:
```
kubectl get svc -n monitoring
NAME                    TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
alertmanager-main       ClusterIP   10.254.220.101   <none>        9093/TCP                     3m20s
alertmanager-operated   ClusterIP   None             <none>        9093/TCP,9094/TCP,9094/UDP   3m20s
blackbox-exporter       ClusterIP   10.254.41.39     <none>        9115/TCP,19115/TCP           3m20s
grafana                 ClusterIP   10.254.226.247   <none>        3000/TCP                     3m19s
kube-state-metrics      ClusterIP   None             <none>        8443/TCP,9443/TCP            3m19s
node-exporter           ClusterIP   None             <none>        9100/TCP                     3m18s
prometheus-adapter      ClusterIP   10.254.193.17    <none>        443/TCP                      3m18s
prometheus-k8s          ClusterIP   10.254.92.43     <none>        9090/TCP                     3m17s
prometheus-operated     ClusterIP   None             <none>        9090/TCP                     3m17s
prometheus-operator     ClusterIP   None             <none>        8443/TCP                     9m4s
```

On utilise le proxy kube pour acceder a l'interface de grafana:
```
kubectl --namespace monitoring port-forward svc/grafana 3000
```

Puis on se connecte a grafana en local via http://localhost:3000 
username : admin

Mot de passe : admin

### Logging
 Installation et configuration d'une suite permettant de récupérer et de stocker de manière persistante les

logs de chaque container du cluster : Loki Stack Configuration du Grafana précédemment installé :

ajout de la datasource Loki
 mise en place d'un dashboard de recherche rapide dans les logs

**4 points**

### Registry

Mise en place d'un registry docker.
 Installation et configuration d'un GUI et d'une gestion des utilisateurs et des droits pour ce registry. Exemple :

Harbor

Bonus:
 scan de vulnérabilité lors du push d'une nouvelle image -> **1 point en plus**

**4 points**

### Wordpress
 Mise en place d'un Wordpress redondé (minimum 2 replica pour le wordpress). Exemple :

bitnami/wordpress

```
creation d'un mot de passe secret 
secretGenerator:
- name: mysql-pass
  literals:
  - password=mypassword



# yaml deploiement de mysql et wordpress (1 service en plus = mysql)           
curl -LO https://k8s.io/examples/application/wordpress/mysql-deployment.yaml
curl -LO https://k8s.io/examples/application/wordpress/wordpress-deployment.yaml


# ajouter les deux ficher dans le kustomization.yaml
vim kustomization.yaml
resources:
  - mysql-deployment.yaml
  - wordpress-deployment.yaml


# en utilisant EOF 
cat <<EOF >>./kustomization.yaml
resources:
  - mysql-deployment.yaml
  - wordpress-deployment.yaml
EOF


#appliquer le deploiement : 
kubectl apply -k ./


#Résultats attendu :
secret/mysql-pass-7t7g9k6f85 created
service/wordpress created
service/wordpress-mysql created
persistentvolumeclaim/mysql-pv-claim created
persistentvolumeclaim/wp-pv-claim created
deployment.apps/wordpress created
deployment.apps/wordpress-mysql created


# Secret 
root@ubuntu:~# kubectl get secrets
NAME                    TYPE                                  DATA   AGE
default-token-6qf5j     kubernetes.io/service-account-token   3      3d20h
mysql-pass-7t7g9k6f85   Opaque                                1      54s


# PersistentVolume 20Go de chaque
root@ubuntu:~# kubectl get pvc
NAME             STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
mysql-pv-claim   Bound    pvc-4122f146-4324-4515-af2b-dc04d0e10fb8   20Gi       RWO            local-path     67s
wp-pv-claim      Bound    pvc-5d5e46e8-289c-459d-b492-094b86eb9865   20Gi       RWO            local-path     67s


# Verification des pods et des pods
kubectl get pods
kubectl get services wordpress


# Obtenir l'ip du service 
minikube service wordpress --url
```
```
root@ubuntu:~# kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep ${SA_NAME} | awk '{print $1}')

root@ubuntu:~# SA_NAME="administrateur"
kubectl create rolebinding ops-edit --clusterrole edit --user ops --namespace default

rolebinding.rbac.authorization.k8s.io/ops-edit created

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: dev
  namespace: default
EOF

kubectl get rolebindings --namespace demo

kubectl get roles --namespace default

kubectl get serviceaccounts --namespace default

kubectl describe sa dev -n default
```
```
# Replicas du wordpress
kubectl scale deployments/wordpress --replicas=2
kubectl create namespace harbor
```

**4 points**

### Service au choix
 Ajout d'un service différent supplémentaire sur le cluster.
 **2 à 6 points par service supplémentaire suivant sa complexité technique**

















